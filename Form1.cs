﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string user = textBox1.Text;
            string pass = textBox2.Text;
            string stringFromText;
            bool access = false;

            using (FileStream stream = File.Open("Users.txt", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    while ((stringFromText = reader.ReadLine()) != null)
                    {
                        string[] spliting = stringFromText.Split(',');

                        if (user.CompareTo(spliting[0]) == 0)
                        {
                            access = true;
                        }

                        if(pass.CompareTo(spliting[1]) == 0)
                        {
                            access = true;
                        }
                    }
                }
            }
              
            if(!access)
            {
                MessageBox.Show("your password or user is incorrect");
            }

            else
            {
                Form window = new Form2 { };
                window.Show();
                this.Hide();
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
